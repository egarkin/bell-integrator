package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.Date;

@Entity
@Table(name = "document")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
public class Document extends BaseModel {

    @Version
    private Integer version;

    @ManyToOne
    @JoinColumn(name = "doc_type_id")
    private DocType docType;

    @Column(name = "number", length = 10, nullable = false, unique = true)
    private String number;

    @Column(name = "issue_date", nullable = false)
    private Date issueDate;
}
