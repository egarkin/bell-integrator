package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "user")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
public class User extends BaseModel {

    @Version
    private Integer version;

    @Column(name = "first_name", length = 100, nullable = false)
    private String firstName;

    @Column(name = "second_name", length = 100)
    private String secondName;

    @Column(name = "middle_name", length = 100)
    private String middleName;

    @Column(name = "position", length = 100, nullable = false)
    private String position;

    @Column(name = "phone", length = 11)
    private String phone;

    @OneToOne
    @JoinColumn(name = "document_id")
    @Column(unique = true)
    private Document document;

    @OneToMany
    @JoinColumn(name = "country_id")
    private Country country;

    @Column(name = "is_identified")
    private Boolean isIdentified;
}
