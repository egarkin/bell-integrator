INSERT INTO doc_type (id, code, name)
VALUES (1, 21, 'Паспорт гражданина РФ');

INSERT INTO country (id, code, name)
VALUES (1, 643, 'Российская Федерация');

INSERT INTO document (id, version, doc_type_id, number, issue_date)
VALUES (1, 1, 1, '4501234561', CURDATE()),
       (2, 1, 1, '4501234562', CURDATE() - 1),
       (3, 1, 1, '4501234563', CURDATE() - 2),
       (4, 1, 1, '4501234564', CURDATE() - 3),
       (5, 1, 1, '4501234565', CURDATE() - 4);

INSERT INTO organization (id, version, name, full_name, inn, kpp, address, phone, is_active)
VALUES (1, 1, 'БЕЛЛ ИНТЕГРАТОР', 'АО БЕЛЛ ИНТЕГРАТОР', '7714923230', '771401001', 'г. Москва, ул. Планетная, д. 11 пом. 9/10 РМ-4', '74959806181', true),
       (2, 1, 'Yandex', 'ООО ЯНДЕКС', '7736207543', '770401001', 'г. Москва, ул. Льва Толстого, д. 16', '88002509639', true);

INSERT INTO office (id, version, org_id, name, address, phone, is_active)
VALUES (1, 1, 1, 'Московский офис', '115088, г. Москва 2-й Южнопортовый проезд 18, стр. 2', '74959806181', true),
       (2, 1, 2, 'Главный офис яндекса', '119021, Москва, ул. Льва Толстого, 16', '74957397000', true),
       (3, 1, 2, 'Офис яндекса в Москва-Сити', '123112, Москва, 1-й Красногвардейский проезд, д. 19', '74957397000', true);

INSERT INTO user (id, version, office_id, first_name, second_name, middle_name, position, phone, document_id, country_id, is_identified)
VALUES (1, 1, 1, 'Иванов', 'Петр', 'Сергеевич', 'Junior Java Developer', '79031110022', 1, 1, true),
       (2, 1, 2, 'Петров', 'Сергей', 'Иванович', 'Middle iOS Developer', '79052221133', 2, 1, true),
       (3, 1, 3, 'Сергеев', 'Павел', 'Анатольевич', 'Senior Python Developer', '79163332244', 3, 1, true),
       (4, 1, 1, 'Павлов', 'Иван', 'Евгеньевич', 'QA Team Lead', '79654443355', 4, 1, true),
       (5, 1, 2, 'Егоров', 'Евгений', 'Викторович', 'Project Manager', '79775554466', 5, 1, true);