CREATE TABLE IF NOT EXISTS organization
(
    id        INTEGER               COMMENT 'Уникальный идентификатор' PRIMARY KEY AUTO_INCREMENT,
    version   INTEGER      NOT NULL COMMENT 'Служебное поле hibernate',
    name      VARCHAR(100) NOT NULL COMMENT 'Название организации',
    full_name VARCHAR(255) NOT NULL COMMENT 'Полное название организации',
    inn       VARCHAR(10)  NOT NULL COMMENT 'Инн',
    kpp       VARCHAR(9)   NOT NULL COMMENT 'Кпп',
    address   VARCHAR(255) NOT NULL COMMENT 'Юридический адрес',
    phone     VARCHAR(11)           COMMENT 'Телефон организации',
    is_active BOOLEAN               COMMENT 'Работает'
);
COMMENT ON TABLE organization IS 'Организация';

CREATE TABLE IF NOT EXISTS office
(
    id        INTEGER               COMMENT 'Уникальный идентификатор' PRIMARY KEY AUTO_INCREMENT,
    version   INTEGER      NOT NULL COMMENT 'Служебное поле hibernate',
    org_id    INTEGER      NOT NULL COMMENT 'Идентификатор организации',
    name      VARCHAR(100) NOT NULL COMMENT 'Название офиса',
    address   VARCHAR(255) NOT NULL COMMENT 'Адрес офиса',
    phone     VARCHAR(11)           COMMENT 'Телефон офиса',
    is_active BOOLEAN               COMMENT 'Работает'
);
COMMENT ON TABLE office IS 'Офис';

CREATE TABLE IF NOT EXISTS user
(
    id            INTEGER               COMMENT 'Уникальный идентификатор' PRIMARY KEY AUTO_INCREMENT,
    version       INTEGER      NOT NULL COMMENT 'Служебное поле hibernate',
    office_id     INTEGER      NOT NULL COMMENT 'Идентификатор офиса',
    first_name    VARCHAR(100) NOT NULL COMMENT 'Имя сотрудника',
    second_name   VARCHAR(100)          COMMENT 'Фамилия сотрудника',
    middle_name   VARCHAR(100)          COMMENT 'Отчество сотрудника',
    position      VARCHAR(100) NOT NULL COMMENT 'Должность',
    phone         VARCHAR(11)           COMMENT 'Телефон сотрудника',
    document_id   INTEGER      NOT NULL COMMENT 'Идентификатор документа',
    country_id    INTEGER      NOT NULL COMMENT 'Идентификатор страныгражданства',
    is_identified BOOLEAN               COMMENT 'Идентифицирован'
);
COMMENT ON TABLE user IS 'Сотрудник';

CREATE TABLE IF NOT EXISTS country
(
    id   INTEGER               COMMENT 'Уникальный идентификатор' PRIMARY KEY AUTO_INCREMENT,
    code INTEGER      NOT NULL COMMENT 'Код страны',
    name VARCHAR(100) NOT NULL COMMENT 'Гражданство'
);
COMMENT ON TABLE country IS 'Гражданство';

CREATE TABLE IF NOT EXISTS document
(
    id          INTEGER              COMMENT 'Уникальный идентификатор' PRIMARY KEY AUTO_INCREMENT,
    version     INTEGER     NOT NULL COMMENT 'Служебное поле hibernate',
    doc_type_id INTEGER     NOT NULL COMMENT 'Идентификатор типа документа',
    number      VARCHAR(10) NOT NULL COMMENT 'Номер документа',
    issue_date  DATE        NOT NULL COMMENT 'Дата выдачи'
);
COMMENT ON TABLE document IS 'Документ';

CREATE TABLE IF NOT EXISTS doc_type
(
    id   INTEGER               COMMENT 'Уникальный идентификатор' PRIMARY KEY AUTO_INCREMENT,
    code INTEGER      NOT NULL COMMENT 'Код документа',
    name VARCHAR(100) NOT NULL COMMENT 'Название документа'
);
COMMENT ON TABLE doc_type IS 'Тип документа';

CREATE INDEX IX_office_org_id ON office (org_id);
ALTER TABLE office ADD FOREIGN KEY (org_id) REFERENCES organization (id);

CREATE INDEX IX_user_country_id ON user (country_id);
CREATE INDEX IX_user_office_id ON user (office_id);
CREATE UNIQUE INDEX UX_user_document_id ON user (document_id);
ALTER TABLE user ADD FOREIGN KEY (country_id) REFERENCES country (id);
ALTER TABLE user ADD FOREIGN KEY (office_id) REFERENCES office (id);
ALTER TABLE user ADD FOREIGN KEY (document_id) REFERENCES document (id);

CREATE UNIQUE INDEX UX_country_name ON country (name);

CREATE INDEX IX_document_doc_type_id ON document (doc_type_id);
CREATE UNIQUE INDEX UX_document_number ON document (number);
ALTER TABLE document ADD FOREIGN KEY (doc_type_id) REFERENCES doc_type (id);

CREATE UNIQUE INDEX UX_document_type_name ON doc_type (name);